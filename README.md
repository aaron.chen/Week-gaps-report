# Week-Gaps-report

Note that this is actually an app script bound to the Week Gaps/Week Gaps google sheet. If you want to use the code yourself, you need to replace the OUTPUT_FOLDER_ID variable in the genMonth and genFullReport functions and replace the DATA_ANALYTICS_FOLDER_ID in the getClassesThisMonth function.

If you want **run** the code, you need to go to the Week Gaps/Week Gaps google sheet, wait for it to load, and click "Custom Scripts." From here you can generate a full report of the sheet, or a report using a specific month. If there are no week gaps in a month, the report may end up looking weird.

Note that the Week Gaps sheet does not update by itself. Someone on the team has to add rows themselves and add tags.

###Dependencies
The only sheet it looks at in "Week Gaps" is the "Data" sheet.

For the full report, it looks for particular cells (not the data rows, but the information to the side). You can see what cells these are in the getFullReport function.

For the monthly report, it looks for the "D" column (it assumes this to be the "Start of Gap" column), and only considers rows where the "Start of Gap" date is within the month it's looking for. It assumes the "G","H", and "I" columns are used for tagging, and so will use these to calculate which are the top three tags within a month. Finally, to calculate how many classes there were total within a month, it looks for the latest data analytics sheet in the data analytics folder, goes to the "One-on-One" sheet, and counts how many classes started within the specified month.
