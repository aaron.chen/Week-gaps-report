// DO NOT MESS AROUND WITH THE LAYOUT OF THE "Week Gaps" SPREADSHEET EXCEPT TO ADD ROWS.
// IF YOU DO, YOU'RE GOING TO HAVE TO ADJUST THIS SCRIPT
function onOpen() {
  var ui = SpreadsheetApp.getUi();
  ui.createMenu('Custom Scripts')
      .addItem('Generate Full Report', 'genFullReport')
      .addSubMenu(ui.createMenu('Generate Monthly Report')
          .addItem('This Month', 'genThisMonth')
          .addItem('Last Month', 'genLastMonth')
          .addItem('Custom Month', 'promptMonth'))
      .addToUi();
  
  //  SpreadsheetApp.getActive().addMenu("Scripts", [{name: "Generate Full Report", functionName: "genFullReport"}]);
}
function promptMonth(){
  var ui = SpreadsheetApp.getUi();
  var month_res = ui.prompt('Month','What Month Number? (1-12) ',ui.ButtonSet.OK_CANCEL);
  if (month_res.getSelectedButton() !== ui.Button.OK){
    return;
  }
  var month_no = parseInt(month_res.getResponseText());
  if (month_no < 1 || month_no > 12){
    ui.alert("Invalid Month.");
    return;
  }
  var year_res = ui.prompt('Year','What year? (e.g. 2015) ',ui.ButtonSet.OK_CANCEL);
  if (year_res.getSelectedButton() !== ui.Button.OK){
    return;
  }
  var year_no = parseInt(year_res.getResponseText());
  if (year_no < 2015){
    ui.alert("Invalid Year.");
    return;
  }
  genMonth(month_no,year_no);
}
function genLastMonth() {
  var d = new Date();
  genMonth(parseInt(d.getMonth()),parseInt(d.getFullYear()));
}
function genThisMonth(){
  var d = new Date();
  genMonth(parseInt(d.getMonth())+1,parseInt(d.getFullYear()));
}
function genMonth(month,year){
  var d = new Date();
  var dateString = month +" "+ year;
  var doc = DocumentApp.create("Week Gaps Month Report " + dateString);
  var docFile = DriveApp.getFileById(doc.getId());
  var body = doc.getBody();
  
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName("Data");

  var par = body.appendParagraph(getMonthReport(sheet,month,year));
  var style = {};
  style[DocumentApp.Attribute.FONT_FAMILY] = 'Calibri';
  par.setAttributes(style);
 
  var OUTPUT_FOLDER_ID = ""; 
  var thisFolder = DriveApp.getFolderById(OUTPUT_FOLDER_ID);
  thisFolder.addFile(docFile);
  doc.saveAndClose();
  SpreadsheetApp.getUi().alert("Week Gaps Month Report " + dateString + " generated!\n"+doc.getUrl());
}
function genFullReport() {
  
  var OUTPUT_FOLDER_ID = ""; 
  var ui = SpreadsheetApp.getUi();
  ui.alert("When the script is done, the report will be found in this folder: "+OUTPUT_FOLDER_ID);
  
  var d = new Date();
  var dateString = (d.getMonth()+1).toString() +"-"+ d.getDate().toString() +"-"+ d.getFullYear().toString();
  var doc = DocumentApp.create("Week Gaps Full Report " + dateString);
  var docFile = DriveApp.getFileById(doc.getId());
  var body = doc.getBody();
  
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName("Data");
  
  var par = body.appendParagraph(getFullReport(sheet));
  var style = {};
  style[DocumentApp.Attribute.FONT_FAMILY] = 'Calibri';
  par.setAttributes(style);
  var charts = sheet.getCharts();
  for (var i = 0; i < charts.length; i++) {
    body.appendImage(charts[i].getBlob());
  }

  var thisFolder = DriveApp.getFolderById(OUTPUT_FOLDER_ID);
  thisFolder.addFile(docFile);
  doc.saveAndClose();
}

function getFullReport(sheet){
  var totalSessions = sheet.getRange("N3").getValue();
  var totalLapses = sheet.getRange("N6").getValue();
  var adjustedLapses = sheet.getRange("N8").getValue();
  var studentLapseRateFail = sheet.getRange("M39").getValue();
  var studentLapseRateNoFail = sheet.getRange("O39").getValue();
  var student_no = sheet.getRange("N39").getValue();
  var mentor_no = sheet.getRange("N40").getValue();
  var student_commit = sheet.getRange("L18").getValue();
  var student_lazy = sheet.getRange("L23").getValue();
  var mentor_swap = sheet.getRange("L28").getValue();
  var mentor_commit = sheet.getRange("L29").getValue();

  var d = new Date();
  var nl = "\n";
  var dateString = d.getMonth().toString() +"-"+ d.getDate().toString() +"-"+ d.getFullYear().toString();
  t = dateString + nl + nl;
  t += "Of the " + totalSessions + " sessions held so far, there were " + totalLapses + " instances of a course lapsing for a week. Those that lapsed for two or three weeks were counted. Those for more were not. When we account for failures like no-shows (failure on mentor part) and operational failures, we get " + adjustedLapses + " gaps." + nl+nl;
  t += "With these failures, students account for " + studentLapseRateFail + "% of the gaps. Without those included (which is how we should be thinking about it, because student gaps produce revenues), they're only " + studentLapseRateNoFail + "%. See charts below. Essentially, the ratio of of students to mentor caused gaps is " + student_no + ":"+ mentor_no + "." + nl+nl;
  t += "Prior commitments are a huge source of gaps on either side and we can't change that, but there are a few that we can change."+nl+nl;
  t += "1) Students - unforeseen commitments"+nl;
  t += "-"+student_commit+" of "+student_no+" gaps. We can penalize gaps in courses that aren't declared before the course begins. What that penalty is can vary. In any solution though, we have to ask if this will anger the student. Perhaps a more moderate solution is that they have to make up the class before the original course end date lest they lose it. Extending courses might mean a future mentor swap."+nl+nl;
  t += "2) Students - as needed use"+nl;
  t += "-"+student_lazy+" of "+student_no+" Some students book courses but then only use us from time-to-time to study for tests. Do we want this to be our business model? It's time intensive for mentors and us to deal with irregular students, but less so if we aren't responsible for the materials."+nl+nl;
  t += "3) Mentor - mentor swaps"+nl;
  t += "-"+mentor_swap+" of "+mentor_no+". Mentor swaps are when we have to change the mentor mid-course and that slows down completion. See my comment on unforeseen commitments as one possible solution. Maybe Jamey can comment on what the other cases were and how these can be reduced."+nl+nl;
  t += "4) Mentor - unforeseen commitment"+nl;
  t += "-"+mentor_commit+" of "+mentor_no+". Perhaps this can be reduced in the same way as with students, with the caveat that if the student is unable or unwilling to fit the class in before the course was original supposed to end, we give them another mentor for the week after."+nl+nl;
  t += "5) Reduce ops failures and mentor no-shows"+nl+nl;
  t += "Obvious ones here. Mentor no-shows are spread across multiple mentors, so this isn't a particularly easy solve. Ops failures can be reduced certainly and have been decreasing over time.";
  return t;
}

function percent(number){
  return (100.0*number).toString().substring(0,4);
}

function getMonthReport(sh,month,year){
  var d = new Date();
  var nl = "\n";
  var relevantRows = []; var rowDate;
  for (var r = 2; r <= sh.getLastRow(); r++){
    rowDate = sh.getRange("D"+r).getValue();
    if ((parseInt(rowDate.getMonth())+1 === month) && (parseInt(rowDate.getFullYear()) === year)){
      relevantRows.push(r);
    }
  }

  classesThisMonth = getClassesThisMonth(month,year);

  var dateString = d.getMonth().toString() +"-"+ d.getDate().toString() +"-"+ d.getFullYear().toString();
  t = dateString + nl + nl;
  t += "(Only considering one-on-one classes)"+nl;
  t += "During this month ("+month+"/"+year+"), out of "+classesThisMonth+" classes, there were "+relevantRows.length+" gaps."+nl;
  t += "This means classes with gaps made up "+percent(relevantRows.length/classesThisMonth)+"% of our classes this month."+nl+nl

  t += topThreeTags(sh,relevantRows);

  return t;
}

//rows is a array of row numbers
function topThreeTags(sh,rows){

  var tags = [];
  var tag;
  var total = 0;
  for (var r = 0; r < rows.length; r++){
    tag = sh.getRange("G"+rows[r]).getValue();
    if (!tags[tag]){
      tags[tag] = 0;
    }
    tags[tag]++;
    total++;
    tag = sh.getRange("H"+rows[r]).getValue();
    if (tag !== ""){
      if (!tags[tag]){
        tags[tag] = 0;
      }
      tags[tag]++;
      total++;
    }
    tag = sh.getRange("I"+rows[r]).getValue();
    if (tag !== ""){
      if (!tags[tag]){
        tags[tag] = 0;
      }
      tags[tag]++;
      total++;
    }
  }
  var oneNo = 0; var twoNo = 0; var threeNo = 0;
  var one; var two; var three;
  for (var tag in tags){
    if (tags[tag] > oneNo){
      three = two; threeNo = twoNo;
      two = one; twoNo = oneNo;
      one = tag; oneNo = tags[tag];
    }
    else if (tags[tag] > twoNo){
      three = two; threeNo = twoNo;
      two = tag; twoNo = tags[tag];
    }
    else if (tags[tag] > threeNo){
      three = tag; threeNo = tags[tag];
    }
  }

  var nl = "\n";
  var text = "Top three reasons for gaps:"+nl;
  text += ""+one+": "+oneNo+" instances. "+percent(oneNo/total)+"%"+nl;
  text += ""+two+": "+twoNo+" instances. "+percent(twoNo/total)+"%"+nl;
  text += ""+three+": "+threeNo+" instances. "+percent(threeNo/total)+"%"+nl+nl;
  text += "The complete list is below:"+nl;
  for (var tag in tags){
    text += tag+": "+tags[tag]+" instances. "+percent(tags[tag]/total)+"%"+nl;
  }
  text += nl;
  return text;
}

function getClassesThisMonth(month,year){
    var DATA_ANALYTICS_FOLDER_ID;
    var ss = convertExcel2Sheets(getNewestFile(DATA_ANALYTICS_FOLDER_ID),"Temp " + (new Date()));
    var oneonesh = ss.getSheetByName("One-on-One Classes");
    var dates = oneonesh.getRange(4,8,oneonesh.getLastRow()).getDisplayValues();
    var total = 0; var date;
    for (var r = 0; r < dates.length; r++){
      date = dates[r][0].toString().split("/");
      if ((parseInt(date[0]) === month) && (parseInt(date[2]) === year)){
        total++;
      }
    }
    DriveApp.getFileById(ss.getId()).setTrashed(true);
    return total;
}

function getNewestFile(folder_id) {
  var folder = DriveApp.getFolderById(folder_id);
  var files = folder.getFiles();
  var latest = null;
  while (files.hasNext()){
    var file = files.next();
    if (file.getName().charAt(0) === '~'){
      continue;
    }
    if ((latest === null) || (file.getLastUpdated() > latest.getLastUpdated())){
      latest = file;
    }
  }
  return latest;
}

/**
 * Convert Excel file to Sheets
 * @param {Blob} excelFile The Excel file blob data; Required
 * @param {String} filename File name on uploading drive; Required
 * @param {Array} arrParents Array of folder ids to put converted file in; Optional, will default to Drive root folder
 * @return {Spreadsheet} Converted Google Spreadsheet instance
 **/
function convertExcel2Sheets(excelFile, filename, arrParents) {

  var parents  = arrParents || []; // check if optional arrParents argument was provided, default to empty array if not
  if ( !parents.isArray ) parents = []; // make sure parents is an array, reset to empty array if not

  // Parameters for Drive API Simple Upload request (see https://developers.google.com/drive/web/manage-uploads#simple)
  var uploadParams = {
    method:'post',
    contentType: 'application/vnd.ms-excel', // works for both .xls and .xlsx files
    contentLength: excelFile.getBlob().getBytes().length,
    headers: {'Authorization': 'Bearer ' + ScriptApp.getOAuthToken()},
    payload: excelFile.getBlob().getBytes()
  };

  // Upload file to Drive root folder and convert to Sheets
  var uploadResponse = UrlFetchApp.fetch('https://www.googleapis.com/upload/drive/v2/files/?uploadType=media&convert=true', uploadParams);

  // Parse upload&convert response data (need this to be able to get id of converted sheet)
  var fileDataResponse = JSON.parse(uploadResponse.getContentText());

  // Create payload (body) data for updating converted file's name and parent folder(s)
  var payloadData = {
    title: filename, 
    parents: []
  };
  if ( parents.length ) { // Add provided parent folder(s) id(s) to payloadData, if any
    for ( var i=0; i<parents.length; i++ ) {
      try {
        var folder = DriveApp.getFolderById(parents[i]); // check that this folder id exists in drive and user can write to it
        payloadData.parents.push({id: parents[i]});
      }
      catch(e){} // fail silently if no such folder id exists in Drive
    }
  }
  // Parameters for Drive API File Update request (see https://developers.google.com/drive/v2/reference/files/update)
  var updateParams = {
    method:'put',
    headers: {'Authorization': 'Bearer ' + ScriptApp.getOAuthToken()},
    contentType: 'application/json',
    payload: JSON.stringify(payloadData)
  };

  // Update metadata (filename and parent folder(s)) of converted sheet
  UrlFetchApp.fetch('https://www.googleapis.com/drive/v2/files/'+fileDataResponse.id, updateParams);

  return SpreadsheetApp.openById(fileDataResponse.id);
}